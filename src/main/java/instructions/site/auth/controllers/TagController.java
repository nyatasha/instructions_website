package instructions.site.auth.controllers;

import instructions.site.auth.Dao.TagDao;
import instructions.site.auth.model.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by rodya on 3.3.17.
 */
@Controller
public class TagController {
    @Autowired
    TagDao tagDao;

    @RequestMapping(value = "/get_tags")
    public
    @ResponseBody
    List<Tag> get_tags() {
        return tagDao.getTags();
    }


    @RequestMapping(value = "/getTagNames")
    @ResponseBody
    public List<String> getTagNames() {
        return tagDao.getTagNames();
    }
}
