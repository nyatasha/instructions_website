package instructions.site.auth.controllers;

import instructions.site.auth.Dao.UserDao;
import instructions.site.auth.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;

/**
 * Created by Natallia on 20.02.2017.
 */
@Controller
@RequestMapping("/facebook")
public class FacebookController {
    private Facebook facebook;
    private ConnectionRepository connectionRepositoryFacebook;
    @Autowired
    private UserDao userDao;
    public String profileId = "User";
    public String uniqID = "uniqID";

    @Inject
    public FacebookController(Facebook facebook, ConnectionRepository connectionRepository) {
        this.facebook = facebook;
        this.connectionRepositoryFacebook = connectionRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String helloFacebook(Model model) {
        if (connectionRepositoryFacebook.findPrimaryConnection(Facebook.class) == null) {
            return "redirect:/connect/facebook";
        }
        profileId = facebook.userOperations().getUserProfile().getName();
        uniqID = facebook.userOperations().getUserProfile().getId();
        User user = userDao.authUser(profileId, uniqID);
        if (!user.isLocked()) {
            UserController.AddUserAtContext(user, model);
        } else {
            return "banned";
        }
        return "redirect:/home";
    }
}