package instructions.site.auth.controllers;

import instructions.site.auth.Dao.ArticleDao;
import instructions.site.auth.Dao.LikeDao;
import instructions.site.auth.model.Likes;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by rodion on 11.03.2017.
 */
@Controller
public class LikeController {

    @Autowired
    LikeDao likeDao;
    @Autowired
    ArticleDao articleDao;

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/setLike")
    public
    @ResponseBody
    String setLike(long articleId, int value, Model model) {
        long userId = (long) SecurityContextHolder.getContext().getAuthentication().getCredentials();
        Likes like = likeDao.findByUserAndArticle(userId, articleId);
        if (like == null) {
            like = new Likes(articleId, userId, value);
            likeDao.create(like);
        }
        likeDao.setLike(like.getArticleId(), like.getUserId(), value);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("countLikes", likeDao.countLikes(articleId));
            jsonObject.put("countDisLikes", likeDao.countDisLikes(articleId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

}
