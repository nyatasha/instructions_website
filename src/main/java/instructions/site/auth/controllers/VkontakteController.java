package instructions.site.auth.controllers;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import instructions.site.auth.Dao.UserDao;
import instructions.site.auth.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.vkontakte.api.VKontakte;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/vkontakte")
public class VkontakteController {
    @Autowired
    private ConnectionRepository connectionRepository;
    @Autowired
    private UserDao userDao;

    @RequestMapping(method = RequestMethod.GET)
    public String home(Model model) throws ClientException, ApiException {
        Connection<VKontakte> connection = connectionRepository.findPrimaryConnection(VKontakte.class);
        ConnectionKey connectionKey = connection.getKey();
        if (connection == null) {
            return "redirect:/connect/vkontakte";
        }
        try {
            User user = userDao.authUser(connection.getDisplayName(), connection.getProfileUrl());
            if (!user.isLocked()) {
                UserController.AddUserAtContext(user, model);
            } else {
                return "banned";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        connectionRepository.removeConnection(connectionKey);
        return "redirect:/home";
    }

}