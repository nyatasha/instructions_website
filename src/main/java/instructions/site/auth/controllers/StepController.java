package instructions.site.auth.controllers;

import instructions.site.auth.Dao.StepDao;
import instructions.site.auth.model.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Natallia on 02.03.2017.
 */

@Controller
public class StepController {

    @Autowired
    private StepDao stepDao;

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/create_step")
    @ResponseBody
    public String create(String title) {
        try {
            Step step = new Step();
            //step.setIdArticle(123);
            //step.setPosition(123);
            step.setUrl("rtfghjbn");
            step.setStepTitle(title);

            stepDao.create(step);
        } catch (Exception ex) {
            return "Error creating the step: " + ex.toString();
        }
        return "step succesfully created!";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/delete_step")
    @ResponseBody
    public String delete(long id) {
        try {
            Step step = new Step();
            step.setId(id);
            stepDao.delete(step);
        } catch (Exception ex) {
            return "Error deleting the step: " + ex.toString();
        }
        return "step succesfully deleted!";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/update_step")
    @ResponseBody
    public String updateName(long id, String title) {
        try {
            Step step = stepDao.getById(id);

            step.setAllProperties(title);
            stepDao.update(step);
        } catch (Exception ex) {
            return "Error updating the step: " + ex.toString();
        }
        return "step succesfully updated!";
    }

}