package instructions.site.auth.controllers;

import instructions.site.auth.Dao.LikeDao;
import instructions.site.auth.Dao.RoleDao;
import instructions.site.auth.Dao.UserDao;
import instructions.site.auth.model.Roles;
import instructions.site.auth.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodya on 9.3.17.
 */
@Controller
public class UserController {
    @Autowired
    UserDao userDao;
    @Autowired
    LikeDao likeDao;
    @Autowired
    RoleDao roleDao;

    public static void AddUserAtContext(User user, Model model) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Roles role : user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        Authentication authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getId(), authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        model.addAttribute("profileId", authentication.getName());
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/sudo")
    public
    @ResponseBody
    String makeUserAdmin(int id) {
        User user = userDao.getById(id);
        Roles role = roleDao.getRoleByID(1L);
        if (user.getRoles().contains(role)) {
            user.getRoles().remove(role);
        } else {
            user.getRoles().add(role);
        }
        userDao.update(user);
        return "admincReated";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/ban")
    public String banUser(int id) {
        User user = userDao.getById(id);
        if (!user.isLocked()) {
            user.setLocked(true);
            userDao.update(user);
            return "admin";
        } else {
            user.setLocked(false);
            userDao.update(user);
            return "admin";
        }
    }

}