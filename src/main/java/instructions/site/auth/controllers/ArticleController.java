package instructions.site.auth.controllers;

import instructions.site.auth.Dao.ArticleDao;
import instructions.site.auth.Dao.CategoryDao;
import instructions.site.auth.Dao.LikeDao;
import instructions.site.auth.Dao.StepDao;
import instructions.site.auth.model.Article;
import instructions.site.auth.model.Step;
import instructions.site.auth.Dao.*;
import instructions.site.auth.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;
import javax.persistence.Query;
import java.util.*;

@Controller
public class ArticleController {

    @Autowired
    private ArticleDao articleDao;
    @Autowired
    private StepDao stepDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private TagDao tagDao;
    @Autowired
    private LikeDao likeDao;
    private boolean isDeleted = false;
    private boolean isCreated = false;

    @RequestMapping(value = "/get_articleId")
    @ResponseBody
    public String getLastId() {
        String id;
        try {
            Article article = articleDao.getLastId();
            id = String.valueOf(article
                    .getId());
        } catch (Exception ex) {
            return "User not found: " + ex.toString();
        }
        return "The user id is: " + id;
    }

    @RequestMapping(value = "/save_article", method = RequestMethod.POST)
    public
    @ResponseBody
    void saveArticle(@RequestBody String stepsString) throws JSONException {
        JSONObject jsonObjectMain = new JSONObject(stepsString);
        Article article = new Article();
        JSONArray arrayTags = jsonObjectMain.getJSONArray("tags");

        try {
            article.setAllProperties(jsonObjectMain.getString("titleArticle"),
                    categoryDao.getIdByCategory(jsonObjectMain.getString("category")),
                    jsonObjectMain.getString("videoUrl"));
            JSONArray array = jsonObjectMain.getJSONArray("tags");
            JSONObject jsonobject = array.getJSONObject(0);
            String temp = jsonobject.getString("tag");
            String[] tagStrings = temp.split(String.format("%c+", (char)160));

            Set<Tag> tags = new HashSet<Tag>();
            for (int k = 0; k < tagStrings.length; k++) {
                try {
                    Tag tag = new Tag();
                    tag.setTag_name(tagStrings[k]);
                    tagDao.createTag(tag);
                    tags.add(tag);
                    article.setTags(tags);
                } catch (Exception ex) {
                    System.out.println("Error creating the tag: " + ex.toString());
                }
            }
            article.setUserId(Integer.parseInt(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString()));
            //System.out.println(articleDao.getByTitle(jsonObjectMain.getString("titleArticle")));
            articleDao.create(article);

        } catch (Exception ex) {
            System.out.println("Error creating the article: " + ex.toString());
        }
        long userId = (long) SecurityContextHolder.getContext().getAuthentication().getCredentials();
        // Likes like = new Likes(article.getId(), userId, 0);
        //likeDao.create(like);
        JSONArray jArray = jsonObjectMain.getJSONArray("steps");
        for (int i = 0; i < jArray.length(); i++) {
            JSONObject jsonobject = jArray.getJSONObject(i);
            //System.out.println(jsonobject.getString("text"));
            try {
                Step step = new Step();
                System.out.println(article.getId());
                step.setIdArticle(article.getId());
                step.setStepTitle(jsonobject.getString("text"));
                step.setPosition(Long.valueOf(jsonobject.getString("position")).longValue());
                step.setUrl(jsonobject.getString("url"));
                stepDao.create(step);

            } catch (Exception ex) {
                System.out.println("Error creating the step: " + ex.toString());
            }
        }
        isCreated = true;
    }

    @RequestMapping(value = "/article_created", method = RequestMethod.GET)
    String savedArticle(Model model) {
        if (isCreated == true) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            model.addAttribute("profileId", auth.getName());
            isCreated = false;
            return "redirect:/profile";
        }
        return "error";
    }

    @RequestMapping(value = "/edit_article", method = RequestMethod.POST)
    public
    @ResponseBody
    void editArticle(@RequestBody String stepsString) throws JSONException {
        JSONObject jsonObjectMain = new JSONObject(stepsString);
        Article article = new Article();
        try {
            article.setAllProperties(jsonObjectMain.getString("titleArticle"),
                    categoryDao.getIdByCategory(jsonObjectMain.getString("category")),
                    jsonObjectMain.getString("videoUrl"));
            article.setUserId(Integer.parseInt(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString()));
            articleDao.update(article);
        } catch (Exception ex) {
            System.out.println("Error creating the article: " + ex.toString());
        }
        JSONArray jArray = jsonObjectMain.getJSONArray("steps");
        for (int i = 0; i < jArray.length(); i++) {
            JSONObject jsonobject = jArray.getJSONObject(i);
            try {
                Step step = new Step();
                System.out.println(article.getId());
                step.setIdArticle(article.getId());
                step.setStepTitle(jsonobject.getString("text"));
                step.setPosition(Long.valueOf(jsonobject.getString("position")).longValue());
                step.setUrl(jsonobject.getString("url"));
                stepDao.update(step);
            } catch (Exception ex) {
                System.out.println("Error creating the step: " + ex.toString());
            }
        }
    }

    @RequestMapping(value = "/delete_article", method = RequestMethod.POST)
    public
    @ResponseBody
    void deleteArticle(@RequestBody long id) throws JSONException, IOException {
        isDeleted = true;
        likeDao.delete(likeDao.findByUserAndArticle(Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString()), id));
        Set<Tag> tagSet = articleDao.getById((int) id).getTags();
        for (Tag tag: tagSet) {
            tagDao.delete(tag);
            tagSet.remove(tag);
        }
        List<Step> stepList = stepDao.getListByArticleId((int) id);
        for (int i = 0; i < stepList.size(); i++) {
            System.out.println("stepId " + stepList.get(i).getId());
            stepDao.delete(stepList.get(i));
        }
        articleDao.delete(articleDao.getById((int) id));
    }
}