package instructions.site.auth.controllers;

import com.itextpdf.text.DocumentException;
import instructions.site.auth.Dao.*;
import instructions.site.auth.model.Article;
import instructions.site.auth.model.Roles;
import instructions.site.auth.model.Step;
import instructions.site.auth.model.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.*;

/**
 * Created by Natallia on 18.02.2017.
 */
@Controller
public class MainController {
    public String profileId = "User";
    public Integer idArticle;
    @Autowired
    private UserDao userDao;
    @Autowired
    RoleDao roleDao;
    @Autowired
    private ArticleDao articleDao;
    @Autowired
    private StepDao stepDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    LikeDao likeDao;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    String home(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("profileId", auth.getName());
        model.addAttribute("articlesList", getLatestArticles());
        try {
            getArticleById(randomizeArticle());
            getPopularArticles();
            model.addAttribute("popularList", getPopularArticles());
            model.addAttribute("randomArticle", getArticleById(randomizeArticle()));
        } catch (NullPointerException e) {
            model.addAttribute("randomArticle", false);
            model.addAttribute("popularList", false);
        }
        return "home";
    }


    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    String profile(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("profileId", auth.getName());
        System.out.println(getListOfUserArticles((long) SecurityContextHolder.getContext().getAuthentication().getCredentials()));
        model.addAttribute("articlesList", getListOfUserArticles(Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString())));
        return "profile";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createInstruction(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("profileId", auth.getName());
        return "create";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/all_articles", method = RequestMethod.GET)
    String showAllArticles(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("profileId", auth.getName());
        model.addAttribute("articleList", getListOfAllArticles());
        return "all_articles";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("profileId", auth.getName());
        model.addAttribute("userList", getUserList());
        return "admin";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/logout_user", method = RequestMethod.GET)
    public String logout() {
        SecurityContextHolder.clearContext();
        return "redirect:/home";
    }


    public String getUserList() {
        List<User> userList = userDao.getAll();
        JSONObject jObject = new JSONObject();
        try {
            JSONArray jArray = new JSONArray();
            for (User user : userList) {
                JSONObject userJSON = new JSONObject();
                userJSON.put("username", user.getUsername());
                userJSON.put("id", user.getId());
                userJSON.put("isLocked", user.isLocked());
                Roles role = roleDao.getRoleByID(1L);
                if (user.getRoles().contains(role)) {
                    userJSON.put("isAdmin", true);
                } else {
                    userJSON.put("isAdmin", false);
                }

                jArray.put(userJSON);
            }
            jObject.put("userList", jArray);
        } catch (Exception jse) {
            return "Error getting users " + jse.toString();
        }
        return jObject.toString();
    }

    public String getLatestArticles() {
        List<Article> sortedList = articleDao.getSortedByDate();

        List<Article> articleList = new ArrayList<>(sortedList);
        if (sortedList.size() > 4) {
            articleList = new ArrayList<>(sortedList.subList(0, 4));
        }
        return getRequestedArticles(articleList);
    }

    String getRequestedArticles(List<Article> articleList) {
        JSONObject jObject = new JSONObject();
        try {
            JSONArray jArray = new JSONArray();
            for (Article article : articleList) {
                List<Step> stepList = stepDao.getListByArticleId(article.getId());
                JSONObject userJSON = new JSONObject();
                userJSON.put("id", article.getId());
                userJSON.put("title", article.getTitle());
                if (stepList.get(stepList.size() - 1).getUrl() != null) {
                    userJSON.put("img", stepList.get(stepList.size() - 1).getUrl());
                } else {
                    userJSON.put("img", "http://placehold.it/150x150");
                }
                jArray.put(userJSON);
            }
            jObject.put("articleList", jArray);
        } catch (Exception jse) {
            return "Error getting users " + jse.toString();
        }
        return jObject.toString();
    }

    public String getPopularArticles() {
        List<Article> articleList = articleDao.getAll();
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        JSONObject jObject = new JSONObject();
        for (Article article : articleList) {
            hashMap.put(article.getId(), likeDao.countLikes(article.getId()));
        }
        HashMap<Integer, Integer> sortedMap = sortHashMapByValues(hashMap);
        List<Article> sortedList = new ArrayList<>();
        int count = 0;
        for (Integer key : sortedMap.keySet()) {
            sortedList.add(articleDao.getById(key));
            count++;
            if (count == 4) {
                break;
            }
        }
        return getRequestedArticles(sortedList);
    }

    public LinkedHashMap<Integer, Integer> sortHashMapByValues(
            HashMap<Integer, Integer> passedMap) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Integer> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap =
                new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Integer comp1 = passedMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }

    public String getArticleById(int articleId) {
        List<Step> stepList = stepDao.getListByArticleId(articleId);
        JSONObject jObject = new JSONObject();
        try {
            JSONArray jArray = new JSONArray();
            for (Step step : stepList) {
                JSONObject userJSON = new JSONObject();
                userJSON.put("title", step.getStepTitle());
                userJSON.put("img", step.getUrl());
                jArray.put(userJSON);
            }
            int countLikes = likeDao.countLikes(articleId);
            int countDisLikes = likeDao.countDisLikes(articleId);
            jObject.put("author", userDao.getById(articleDao.getById(articleId).getUserId()).getUsername());
            jObject.put("countLikes", countLikes);
            jObject.put("countDislikes", countDisLikes);
            jObject.put("articleid", articleId);
            jObject.put("category", categoryDao.getById(articleDao.getById(articleId).getCategory()).getCategory_en());
            jObject.put("articleTitle", articleDao.getById(articleId).getTitle());
            jObject.put("videoURL", articleDao.getById(articleId).getvideoURL());
            jObject.put("date", articleDao.getById(articleId).getDate());
            jObject.put("stepList", jArray);
        } catch (Exception jse) {
            return "Error getting users " + jse.toString();
        }
        return jObject.toString();
    }

    public String getListOfUserArticles(long userId) {
        List<Article> articleList = articleDao.getByUserId(userId);
        return getListOfRequestedArticles(articleList);
    }

    public String getListOfRequestedArticles(List<Article> articleList) {
        JSONObject jObject = new JSONObject();
        try {
            JSONArray articlesArray = new JSONArray();
            for (Article article : articleList) {
                JSONObject jsonObj = new JSONObject(getArticleById(article.getId()));
                articlesArray.put(jsonObj);
                jObject.put("articleList", articlesArray);
            }
        } catch (Exception jse) {
            return "Error getting articles " + jse.toString();
        }
        return jObject.toString();
    }

    public String getListOfAllArticles() {
        List<Article> articleList = articleDao.getAll();
        return getListOfRequestedArticles(articleList);
    }

    public int randomizeArticle() {
        Random r = new Random();
        List<Article> allArticles = articleDao.getAll();
        if (allArticles.size() != 0) {
            int Result = r.nextInt(allArticles.size());
            return allArticles.get(Result).getId();
        } else {
            return 0;
        }
    }

    @RequestMapping(value = "/show_article", method = RequestMethod.POST)
    public
    @ResponseBody
    void addArticleId(@RequestBody int id) throws JSONException {
        idArticle = new Integer(id);
    }

    @RequestMapping(value = "/article", method = RequestMethod.GET)
    public String showArticle(Model model) {
        if (idArticle != null) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            model.addAttribute("profileId", auth.getName());
            model.addAttribute("isAdmin", auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN")));
            model.addAttribute("articleList", getArticleById(idArticle));
            return "article";
        }
        return "Error!";
    }



    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("profileId", auth.getName());
        model.addAttribute("article", getArticleById(idArticle));
        return "edit";
    }
}