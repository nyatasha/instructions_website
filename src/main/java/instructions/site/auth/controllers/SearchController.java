package instructions.site.auth.controllers;

import instructions.site.auth.Dao.StepDao;
import instructions.site.auth.model.Article;
import instructions.site.auth.model.Step;
import instructions.site.auth.search.ArticleSearch;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodya on 13.3.17.
 */
@Controller
public class SearchController {
    @Autowired
    MainController mainController;
    @Autowired
    ArticleSearch articleSearch;
    @Autowired
    StepDao stepDao;

    @RequestMapping("/search")
    public
    @ResponseBody
    String search(String q, Model model) {
        List<Article> searchResults= new ArrayList<>();
        try {
            searchResults = (List<Article>) articleSearch.search(q);
        } catch (Exception ex) {
        }
        if (searchResults.size() != 0) {
            return listToString(searchResults);
        }else{
            return "null";
        }
    }


    public String listToString(List<Article> resultsSearch){
        if(resultsSearch!=null){
        JSONObject jObject = new JSONObject(resultsSearch);
        List<Article> articleList = new ArrayList<>(resultsSearch);
        try {
            JSONArray jArray = new JSONArray();
            for (Article article : articleList) {
                List<Step> stepList = stepDao.getListByArticleId(article.getId());
                JSONObject userJSON = new JSONObject();
                userJSON.put("id", article.getId());
                userJSON.put("title", article.getTitle());
                if (stepList.get(stepList.size() - 1).getUrl() == null) {
                    userJSON.put("img", "http://placehold.it/150x150");
                } else {
                    userJSON.put("img", stepList.get(stepList.size() - 1).getUrl());
                }
                jArray.put(userJSON);
            }
            jObject.put("articleList", jArray);
        } catch (Exception jse) {
            return "Error getting users " + jse.toString();
        }
        return jObject.toString();
        }else{return null;}
    }
}
