package instructions.site.auth.controllers;

import instructions.site.auth.Dao.CategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by rodya on 7.3.17.
 */
@Controller
public class CategoryController {
    @Autowired
    CategoryDao categoryDao;

    @RequestMapping(value = "/getCategoriesEn")
    public
    @ResponseBody
    List<String> getCategoriesEn() {
        return categoryDao.getCategoriesEn();
    }
}
