package instructions.site.auth.controllers;

import instructions.site.auth.Dao.UserDao;
import instructions.site.auth.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;

/**
 * Created by Natallia on 20.02.2017.
 */
@Controller
@RequestMapping("/twitter")
public class TwitterController {
    @Autowired
    private UserDao userDao = new UserDao();
    private Twitter twitter;
    private ConnectionRepository connectionRepositoryTwitter;
    public String profileId = "User";
    public String uniqID = "http://twitter.com/";

    @Inject
    public TwitterController(Twitter twitter, ConnectionRepository connectionRepository) {
        this.twitter = twitter;
        this.connectionRepositoryTwitter = connectionRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String helloTwitter(Model model) {
        if (connectionRepositoryTwitter.findPrimaryConnection(Twitter.class) == null) {
            return "redirect:/connect/twitter";
        }
        profileId = twitter.userOperations().getUserProfile().getName();
        uniqID = twitter.userOperations().getUserProfile().getProfileUrl();
        User user = userDao.authUser(profileId, uniqID);
        if (!user.isLocked()) {
            UserController.AddUserAtContext(user, model);
        } else {
            return "banned";
        }
        return "redirect:/home";
    }


    @ModelAttribute("profileId")
    public String messages() {
        return profileId;
    }
}
