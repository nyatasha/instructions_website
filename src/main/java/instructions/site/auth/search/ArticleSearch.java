package instructions.site.auth.search;

import instructions.site.auth.model.Article;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by rodya on 13.3.17.
 */
@Repository
@Transactional
public class ArticleSearch {
@PersistenceContext
    private EntityManager entityManager;

public List search(String text){
    FullTextEntityManager fullTextEntityManager=
            org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);

    QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory()
            .buildQueryBuilder().forEntity(Article.class).get();
org.apache.lucene.search.Query query = queryBuilder.keyword().onField("title").matching(text).createQuery();
    org.hibernate.search.jpa.FullTextQuery jpaQuery =
            fullTextEntityManager.createFullTextQuery(query, Article.class);
    @SuppressWarnings("unchecked")
            List results = jpaQuery.getResultList();
    return results;
}
}
