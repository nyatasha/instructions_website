package instructions.site.auth.model;

//import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by rodya on 24.2.17.
 */
@Entity
@Table(name = "roles")
public class Roles {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
