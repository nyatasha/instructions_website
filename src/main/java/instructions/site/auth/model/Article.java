package instructions.site.auth.model;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "articles")
@Indexed
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Field
    @Column(name = "title")
    private String title;

    @Column(name = "category")
    private int category;

    @Column(name = "user_id")
    private long user_id;

    @Column(name = "date")
    private Date date;

    @Column(name = "videoURL")
    private String videoURL;

    @ManyToMany
    @JoinTable(name = "tagsArticles",
            joinColumns = {@JoinColumn(name = "article_id")},
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags;

    public void setAllProperties(String title, int category, String url) {
        this.date = new Date();
        this.title = title;
        this.category = category;
        this.videoURL = url;
    }

    public String getvideoURL() {
        return videoURL;
    }

    public void setvideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getCategory() {
        return category;
    }

    public void setTitle(String text) {
        this.title = text;
    }

    public long getUserId() {
        return user_id;
    }

    public void setUserId(long id) {
        this.user_id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
}