package instructions.site.auth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by rodya on 7.3.17.
 */
@Entity
@Table(name = "categories")
public class Category {

    @Id
    @Column(name = "category_id")
    private int category_id;

    @Column(name = "category_en")
    private String category_en;

    @Column(name = "category_ru")
    private String category_ru;

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_en() {
        return category_en;
    }

    public void setCategory_en(String category_en) {
        this.category_en = category_en;
    }

    public String getCategory_ru() {
        return category_ru;
    }

    public void setCategory_ru(String category_ru) {
        this.category_ru = category_ru;
    }
}
