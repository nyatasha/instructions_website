package instructions.site.auth.model;

import javax.persistence.*;

@Entity
@Table(name = "steps")
public class Step {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idStep;

    @Column(name = "idArticles")
    private long idArticle;

    @Column(name = "position")
    private long position;

    @Column(name = "url")
    private String url;

    @Column(name = "stepTitle")
    private String title;

    public void setAllProperties(String title) {
        this.idArticle = 123;
        this.position = 123;
        this.url = "google.com";
        this.title = "sdfghj";
    }

    public long getId() {
        return idStep;
    }

    public void setId(long id) {
        this.idStep = id;
    }

    public void setIdArticle(long idArticle) {
        this.idArticle = idArticle;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public String getStepTitle() {
        return title;
    }

    public void setStepTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
