package instructions.site.auth.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "likes")
public class Likes {

    public Likes() {
    }

    ;

    public Likes(long articleId, long userId, int value) {
        this.articleId = articleId;
        this.userId = userId;
        this.value = 0;
    }

    @Id
    @Column(name = "likeId")
    private long likeId;

    @Column(name = "articleId")
    private long articleId;

    @Column(name = "userId")
    private long userId;

    @Column(name = "value")
    private int value;


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getArticleId() {
        return articleId;
    }

    public void setArticleId(long articleId) {
        this.articleId = articleId;
    }

    public long getLikeId() {
        return likeId;
    }

    public void setLikeId(long likeId) {
        this.likeId = likeId;
    }
}
