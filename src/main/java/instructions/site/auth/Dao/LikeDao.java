package instructions.site.auth.Dao;

import instructions.site.auth.model.Likes;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by rodion on 10.03.2017.
 */
@Repository
@Transactional
public class LikeDao {
    @PersistenceContext
    private EntityManager entityManager;


    public Likes findByUserAndArticle(long userId, long articleId) {
        try {
            return (Likes) entityManager.createQuery("select l from Likes l WHERE l.userId LIKE :userId and l.articleId LIKE :articleId", Likes.class)
                    .setParameter("userId", userId).setParameter("articleId", articleId).getResultList().get(0);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public void deleteLikeByUserId(long userId) {
        Query query = entityManager.createQuery("select mylike from Likes mylike where mulike.userId LIKE :userId", Likes.class).setParameter("userId", userId);
        if (query != null) {
            for (Likes like : (List<Likes>) query.getResultList())
                entityManager.remove(like);
        }
    }

    public int islikedByUser(long userId, long articleId) {
        Likes like =
                (Likes) entityManager
                        .createQuery("select l from Likes l WHERE l.userId LIKE :userId and l.articleId LIKE :articleId", Likes.class)
                        .setParameter("userId", userId).setParameter("articleId", articleId).getResultList().get(0);
        return like.getValue();
    }

    public int countLikes(long articleId) {
        Query query = entityManager.createQuery("select likes from Likes likes WHERE likes.articleId LIKE :articleId AND likes.value LIKE :value", Likes.class)
                .setParameter("articleId", articleId).setParameter("value", 1);
        int length = query.getResultList().size();
        return length;
    }

    public int countDisLikes(long articleId) {
        Query query = entityManager.
                createQuery("select likes from Likes likes WHERE likes.articleId LIKE :articleId AND likes.value LIKE :value", Likes.class)
                .setParameter("articleId", articleId).setParameter("value", 2);
        int length = query.getResultList().size();
        return length;
    }

    public void update(Likes like) {
        entityManager.merge(like);
        return;
    }

    public void delete(Likes like) {
        if (like != null) {
            if (entityManager.contains(like))
                entityManager.remove(like);
            else
                entityManager.remove(entityManager.merge(like));
            return;
        }
    }

    public void create(Likes like) {
        entityManager.persist(like);
        return;
    }

    public void setLike(long articleId, long userId, int inValue) {
        Likes like = findByUserAndArticle(userId, articleId);
        int value = like.getValue();
        switch (inValue) {
            case 1: {
                if (like.getValue() == inValue) {
                    like.setValue(0);
                    update(like);
                    break;
                } else {
                    like.setValue(inValue);
                    update(like);
                    break;
                }
            }
            case 2: {
                if (like.getValue() == inValue) {
                    like.setValue(0);
                    update(like);
                    break;
                } else {
                    like.setValue(inValue);
                    update(like);
                    break;
                }
            }
        }
    }
}