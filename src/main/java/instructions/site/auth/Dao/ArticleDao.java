package instructions.site.auth.Dao;

import instructions.site.auth.model.Article;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ArticleDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(Article article) {
        entityManager.persist(article);
        return;
    }

    public void delete(Article article) {
        if (entityManager.contains(article))
            entityManager.remove(article);
        else
            entityManager.remove(entityManager.merge(article));
        return;
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return entityManager.createQuery("from Article").getResultList();
    }

    public Article getById(int id) {
        return entityManager.find(Article.class, id);
    }

    public Article getByTitle(String title) {
        return (Article) entityManager.createQuery(
                "from Article where title LIKE :title",Article.class).setParameter("title", title)
                .getResultList().get(0);
    }

    public List<Article> getSortedByDate() {
        return entityManager.createQuery(
                "from Article order by date desc")
                .getResultList();
    }


    public List<Article> getByUserId(long user_id) {
        return entityManager.createQuery(
                "from Article where user_id LIKE :user_id",Article.class).setParameter("user_id", user_id)
                .getResultList();
    }

    public void update(Article article) {
        entityManager.merge(article);
    }

    public Article getLastId() {
        return (Article) entityManager.createQuery(
                "from articles LAST_INSERT_ID();")
                .getResultList().get(0);
    }
}