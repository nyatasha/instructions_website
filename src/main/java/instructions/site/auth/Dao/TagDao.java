package instructions.site.auth.Dao;

import instructions.site.auth.model.Article;
import instructions.site.auth.model.Tag;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by rodya on 2.3.17.
 */
@Repository
@Transactional
public class TagDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void createTag(Tag tag)
    {
        entityManager.persist(tag);
    }

    public void delete(Tag tag) {
        if (entityManager.contains(tag))
            entityManager.remove(tag);
        else
            entityManager.remove(entityManager.merge(tag));
        return;
    }

    public List<Tag> getTags(){
        return entityManager.createQuery("from Tag").getResultList();
    }

    public List<String> getTagNames() {
        return entityManager.createQuery("select t.tag_name from Tag t").getResultList();
    }
}
