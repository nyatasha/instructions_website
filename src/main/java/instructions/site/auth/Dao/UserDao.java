package instructions.site.auth.Dao;

import instructions.site.auth.model.Roles;
import instructions.site.auth.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
@Transactional
public class UserDao {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private RoleDao roleDao;

    public void create(User user) {
        entityManager.persist(user);
        return;
    }

    public void delete(User user) {
        if (entityManager.contains(user))
            entityManager.remove(user);
        else
            entityManager.remove(entityManager.merge(user));
        return;
    }

    @SuppressWarnings("unchecked")
    public List<User> getAll() {
        return entityManager.createQuery("from User").getResultList();
    }

    public User getById(long id) {
        return entityManager.find(User.class, id);
    }

    public List<User> getListByUnigId(String ID) {
        return entityManager.
                createQuery("Select u from User u where u.uniqID LIKE :ID", User.class).setParameter("ID", ID).getResultList();
    }

    public User authUser(String profileName, String uniqID) {
        List<User> list = getListByUnigId(uniqID);
        if (list.size() == 0) {
            User createdUser = new User();
            Set<Roles> roles = new HashSet<>();
            roles.add(roleDao.getRoleByID(2L));
            createdUser.setRoles(roles);
            createdUser.setLocked(false);
            createdUser.setUniqID(uniqID);
            createdUser.setUsername(profileName);
            this.create(createdUser);
            return createdUser;
        } else {
            return
                    list.get(0);
        }
    }

    public void update(User user) {
        entityManager.merge(user);
    }
}