package instructions.site.auth.Dao;

import instructions.site.auth.model.Step;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Natallia on 02.03.2017.
 */
@Repository
@Transactional
public class StepDao {


    @PersistenceContext
    private EntityManager entityManager;

    public void create(Step step) {
        entityManager.persist(step);
        return;
    }

    public void delete(Step step) {
        if (entityManager.contains(step))
            entityManager.remove(step);
        else
            entityManager.remove(entityManager.merge(step));
        return;
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return entityManager.createQuery("from steps").getResultList();
    }

    public List<Step> getListByArticleId(int idArticles) {
        return entityManager.createQuery("from Step where idArticles LIKE :idArticles",Step.class)
                .setParameter("idArticles", idArticles)
                .getResultList();
    }

    public Step getById(long id) {
        return entityManager.find(Step.class, id);
    }

    public void update(Step step) {
        entityManager.merge(step);
        return;
    }
}