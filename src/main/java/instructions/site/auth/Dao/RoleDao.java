package instructions.site.auth.Dao;

import instructions.site.auth.model.Roles;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Created by rodion on 01.03.2017.
 */
@Repository
@Transactional
public class RoleDao {
    @PersistenceContext
    private EntityManager entityManager;


    public Roles getRoleByID(long ID) {
        return entityManager.find(Roles.class, ID);
    }
}
