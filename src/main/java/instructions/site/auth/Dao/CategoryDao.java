package instructions.site.auth.Dao;

import instructions.site.auth.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by rodya on 7.3.17.
 */
@Repository
@Transactional
public class CategoryDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List<String> getCategoriesEn() {
        return entityManager.createQuery("select c.category_en from Category c").getResultList();
    }

    public int getIdByCategory(String category_en) {
        Category category = (Category) entityManager.createQuery(
                "from Category where category_en =" + "'" + category_en + "'")
                .getResultList().get(0);
        return category.getCategory_id();
    }

    public Category getById(int id) {
        return entityManager.find(Category.class, id);
    }
}
