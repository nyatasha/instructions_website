/**
 * Created by Natallia on 12.03.2017.
 */
// function addUsersArticles(articles){
//     var jsonData = JSON.parse(articles);
//     //console.log(jsonData.articleList.length);
//     //console.log(jsonData.articleList.articleTitle);
//     for (var i = 0; i < jsonData.articleList.length; i++) {
//         console.log(jsonData.articleList[i].articleTitle);
//         var title = jsonData.articleList[i].articleTitle;
//         var id = jsonData.articleList[i].articleid;
//         var date = jsonData.articleList[i].date;
//         for (var j = 0; j < jsonData.articleList[i].stepList.length; j++) {
//
//             var img = jsonData.articleList[i].stepList[j].img;
//
//             var panel = $('<div class="article" style="background-color: rgba(255,255,255,0.5);width:450px;height:450px;text-align:center;border:4px double cornsilk;"/>').appendTo('.userArticles');
//             panel.append('<img onclick="addArticles(this.alt)" src=\"'+img+'\" alt=\"'+id+'\" width="300px" height="300px"/>');
//             var titleText =  $('<h3 style="overflow: hidden;"/>').appendTo(panel);
//             titleText.append(title);
//             titleText.append('<h5 style="float:right;">'+date.toLocaleFormat('%d-%b-%Y') +'</h5>');
//         }
//     }
// }
function addUsersArticles(articles){
    var jsonData = JSON.parse(articles);
    for (var i = 0; i < jsonData.articleList.length; i++) {
        var title = jsonData.articleList[i].articleTitle;
        var id = jsonData.articleList[i].articleid;
        var date = jsonData.articleList[i].date;

        var title =$('<h1 class="open-article text"  onclick="addArticles(this.id)" id=\"'+id+'\" style="margin-bottom:15px;">'+title+'</h1>').appendTo('.articles-container');
        var body = $('<div id="carousel-demo" class="carousel slide" data-ride="carousel" data-interval="2000"/>').appendTo('.articles-container');
        var indicators = $('<ol class="carousel-indicators"/>').appendTo(body);
        var slider = $('<div class="carousel-inner"/>').appendTo(body);

        var img = jsonData.articleList[i].stepList[0].img;
        var titleStep = jsonData.articleList[i].stepList[0].title;

        var indicator = $('<li data-target="#carousel-demo" class="active" data-slide-to=\"'+0+'\"/li>').appendTo(indicators);
        var slider_item = $('<div class="item active">/ ').appendTo(slider);
        slider_item.append('<img src=\"'+jsonData.articleList[i].stepList[0].img+'\"  alt=\"'+id+'\"> ');
        slider_item.append('<div class="carousel-caption"><h3>'+jsonData.articleList[i].stepList[0].title+'</h3></div> ');

        for (var j = 1; j < jsonData.articleList[i].stepList.length; j++) {
            var img = jsonData.articleList[i].stepList[j].img;
            var titleStep = jsonData.articleList[i].stepList[j].title;

            var indicator = $('<li data-target="#carousel-demo" data-slide-to=\"'+j+'\"/li>').appendTo(indicators);
            var slider_item = $('<div class="item">/ ').appendTo(slider);
            slider_item.append('<img onclick="addArticles(this.alt)" src=\"'+img+'\"  alt=\"'+id+'\"> ');
            slider_item.append('<div class="carousel-caption"><h3>'+titleStep+'</h3></div> ');
        }
        body.append('<a class="left carousel-control" href="#carousel-demo" data-slide="prev"><span class="icon-prev"></span></a>');
        body.append('<a class="right carousel-control" href="#carousel-demo" data-slide="next"><span class="icon-next"></span></a>');
    }
}