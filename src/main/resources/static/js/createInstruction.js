$(document).ready(function(){

    var elements = document.getElementsByClassName( 'editor' );
    for ( var i = 0; i < elements.length; ++i ) {
        CKEDITOR.inline( elements[ i ], { /* config for this instance */ } );
    }

    jQuery("#connections #dynamiclist").sortable({
        axis: "y",
        connectWith: "#connections #dynamiclist",
        containment: "parent",
        items: "> li",
        cursor: "move",
        forceHelperSize: true
    });

    jQuery(document).on('click', '#connections #dynamiclist .fa-times', function () {

        jQuery(this).parent().parent().effect("fade", {}, 200, function () {
            jQuery(this).remove();
            jQuery("#connections #dynamiclist").sortable({
                connectWith: "#connections #dynamiclist"
            });
        });


    });

});
$( function() {
    $(".draggable").draggable({
        snap: '#drop',
        snapMode: 'inner',
        snapTolerance: 50,
        revert: 'invalid',
        helper: 'clone',
        appendTo: "body",
        cursor: 'move',
        start: function(event, ui) {
            $(ui.helper).css('width', "100px");
        },
        stop: function(event, ui) {
            $(ui.helper).css('width', "100px");
        }
    });
    $(".panel-img").droppable({
        drop: function(event, ui) {
            $(this).attr("src", ui.draggable.attr("src"));
        }
    });

    $('#editable').editable();
});
//FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
function autoPlayYouTubeModal(){
    var trigger = $("body").find('[data-toggle="modal"]');
    var newSRC = $("#youtube").val();
    var res;
        trigger.click(function() {
            if(newSRC.includes("=") || newSRC.includes("youtu.be") || newSRC.includes("embed")) {
                if (newSRC.includes("=")) {
                    res = "http://www.youtube.com/embed/" + newSRC.split("=")[1];
                }
                else if (newSRC.includes("youtu.be")) {
                    res = "http://www.youtube.com/embed/" + newSRC.split("be/")[1];
                }
                else {
                    res = newSRC;
                }
                //alert(res);
                var theModal = $(this).data("target"),
                    videoSRC = res;//$(this).attr( "data-theVideo" ),
                videoSRCauto = videoSRC + "?autoplay=1";
                $(theModal + ' iframe').attr('src', videoSRCauto);
                $(theModal + ' button.close').click(function () {
                    $(theModal + ' iframe').attr('src', videoSRC);
                });
            }
            else{
                var theModal = $(this).data("target");

                            }
        });
}

function createPanel(){
    var next = $('.draggablePanelList > li').length+1;

    var panel = $('<li style="font-size: 3em;width:100%;" class="row panel"/>').appendTo('.draggablePanelList');
    var span = $('<span style="font-size: .5em;text-align: center;"/>').appendTo(panel);
    span.append('<i class="fa fa-times fa-5x" style="float:right"></i>');
    span.append('<center><img class="panel-img" style="width:40%;overflow: hidden; height:50%;margin-left:10%;" src="" alt="Drop Here!"/></center>');
    var body = $('<center><input class="form-control panel-text" style="width:50%;margin-bottom:20px;" type="text" placeholder="You can add content here."/></center>').appendTo(span);

    $(".panel-img").droppable({
        drop: function(event, ui) {
            $(this).attr("src", ui.draggable.attr("src"));
        }
    });
    // $('.editor1').each( function () {
    //     CKEDITOR.replace( this.id , {
    //     });
    // });
    var elements = $( '.ckeditor' );
    elements.each( function() {
        CKEDITOR.inline( this, {} );
    } );
}

function saveToDB(){
    var ok = false;
    var myImg = document.getElementsByClassName("panel-img");
    for(var i = 0; i < myImg.length; i++){
        if(document.getElementsByClassName("panel-img")[0].src == "http://localhost:8080/create"){
            ok = true;
        }
        else{
            ok = false;
        }
    }
     if(typeof(document.getElementById('focusedInput myTitle').value) === undefined){
        alert("Don't forget to enter title of instruction!");
    }
    else if(typeof($("#youtube").val()) === undefined){
        alert("Don't forget to add video!");
    }
    else if(ok || typeof(document.getElementsByClassName("panel-text")[0].value)===undefined){
        alert("You have to add a title and am image in every step");
    }
    else {
        var myArticle = {
            titleArticle: {},
            videoUrl: {},
            category: {},
            tags: [],
            steps: []
        };
        myArticle.titleArticle =  document.getElementById('focusedInput myTitle').value;
        myArticle.videoUrl = $("#youtube").val();
        myArticle.category = $("#categories :selected").text();
        //alert(myArticle.category);
        var myTags = $(".tag").find('span').text();
        var resTags = myTags.split(' ');
        console.log(resTags);
        for (var i = 0; i < resTags.length; i++) {
            //alert(resTags[i]);
            myArticle.tags.push({
                "tag"  : resTags[i],
            });
        }

        var step = document.getElementsByClassName("panel");
        for (var i = 0; i < step.length; i++) {
            myArticle.steps.push({
                "text"  : step[i].getElementsByClassName("panel-text")[0].value,
                "url"  : step[i].getElementsByClassName("panel-img")[0].src,
                "position" : i
            });
        }
        //alert(JSON.stringify(myArticle));

        $.ajax({
            url: '/save_article',
            type: 'POST',
            data: JSON.stringify(myArticle), //Stringified Json Object
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function(resposeJsonObject){
                //alert(JSON.stringify(myArticle));
            }
        });
         $('#createModal').modal('show');
    }

}


function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
