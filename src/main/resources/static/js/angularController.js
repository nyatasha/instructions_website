app.controller('myController',['$scope', 'translationService', '$http',
function ($scope, translationService, $http){
  $scope.translate = function(){
       translationService.getTranslation($scope, $scope.selectedLanguage);
   };
   // Инициализация
   $scope.selectedLanguage = 'en';
   $scope.translate();
  
}]);