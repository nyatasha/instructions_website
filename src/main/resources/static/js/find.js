function findInsrtuction(){
    var lookingInstr = document.getElementById("searchField").value;
    console.log(lookingInstr);
    if(lookingInstr!=null) {
        var text = $.post("/search?q=" + lookingInstr);
        console.log(text);
        text.done(function (data) {
            var jsonData = JSON.parse(data);
            for (var i = 0; i < jsonData.articleList.length; i++) {
                //alert(jsonData.articleList[i].id);
                var title = jsonData.articleList[i].title;
                var img = jsonData.articleList[i].img;
                var id = jsonData.articleList[i].id;
                var number = i+1;

                var headText = $('<h2 class="text">Search results</h2><hr/>').appendTo('.search-articles');
                var figDiv = $('<div class="freshArticles"/>').appendTo('.search');
                var figure = $('<figure/>').appendTo(figDiv);
                figure.append('<img onclick="addArticles(this.alt)" class="freshImgs" src=\"'+img+'\" alt=\"'+id+'\" width="200" height="200">');
                figure.append('<figcaption width="200px" height="30px">'+title+'</figcaption>');
            }
        });
    }
}