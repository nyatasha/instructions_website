/**
 * Created by Natallia on 11.03.2017.
 */
function updateInstruction(){
    //alert($('#myTitle').val());
    //if($('#myTitle').val()){
    var ok = false;
    var myImg = document.getElementsByClassName("panel-img");
    for(var i = 0; i < myImg.length; i++){
        if(document.getElementsByClassName("panel-img")[0].src == "http://localhost:8080/create"){
            ok = true;
        }
        else{
            ok = false;
        }
    }
    if(typeof(document.getElementById('focusedInput myTitle').value) === undefined){
        alert("Don't forget to enter title of instruction!");
    }
    else if(typeof($("#youtube").val()) === undefined){
        alert("Don't forget to add video!");
    }
    else if(ok || typeof(document.getElementsByClassName("panel-text")[0].value)===undefined){
        alert("You have to add a title and am image in every step");
    }
    else {
        var myArticle = {
            titleArticle: {},
            videoUrl: {},
            steps: []
        };

        myArticle.titleArticle = document.getElementById('focusedInput myTitle').value;
        if ($("#youtube").val().length > 0) {
            myArticle.videoUrl = $("#youtube").val();
        }
        else {
            myArticle.videoUrl = "";
        }
        var step = document.getElementsByClassName("panel");
        for (var i = 0; i < step.length; i++) {
            myArticle.steps.push({
                "text": step[i].getElementsByClassName("panel-text")[0].textContent,
                "url": step[i].getElementsByClassName("panel-img")[0].src,
                "position": i
            });
        }
        //alert(JSON.stringify(myArticle));

        $.ajax({
            url: '/edit_article',
            type: 'POST',
            data: JSON.stringify(myArticle), //Stringified Json Object
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (resposeJsonObject) {
                //alert(JSON.stringify(myArticle));
            }
        });
        $('#updateModal').modal('show');
        //location.href = "profile";
    }

}
function openInstruction(article){
    var jsonData = JSON.parse(article);
    //alert(article);
    document.getElementById('focusedInput myTitle').value = jsonData.articleTitle;
    $("#youtube").val(jsonData.videoURL);
    document.getElementsByClassName('container step')[0].id=jsonData.articleid;
    $("categories select").val(jsonData.category);
    for (var i = 0; i < jsonData.stepList.length; i++) {
        var title = jsonData.stepList[i].title;
        var img = jsonData.stepList[i].img;
        var number = i+1;

        var next = $('.draggablePanelList > li').length+1;

        var panel = $('<li style="font-size: 3em;width:100%;" class="row panel"/>').appendTo('.draggablePanelList');
        var span = $('<span style="font-size: .5em;text-align: center;"/>').appendTo(panel);
        span.append('<i class="fa fa-times fa-5x" style="float:right"></i>');
        span.append('<center><img class="panel-img" style="width:40%;overflow: hidden;height:50%;margin-left:10%;" src=\"'+img+'\" alt="Drop Here!"/></center>');
        var body = $('<center><input class="form-control panel-text" style="width:50%;margin-bottom:20px;" type="text" placeholder="You can add content here."/></center>').appendTo(span);

        $(".panel-text").val(title);
        // $(".panel-text").each( function() {
        //     $(this).val(title);
        // } );

        $(".panel-img").droppable({
            drop: function(event, ui) {
                $(this).attr("src", ui.draggable.attr("src"));
            }
        });
    }
}
