function starting(){
    //alert(document.cookie);
    if(getCookie("navbarTheme") != null)
        document.getElementById("navbar").className = getCookie("navbarTheme");
    else{
        var navbarTheme =  "navbar navbar-default navbar-fixed-top";
        document.getElementById("navbar").className = navbarTheme;
        document.cookie = "navbarTheme="+navbarTheme;
    }
    if(getCookie("bodyBackground") != null)
        document.getElementsByTagName("body")[0].style =  getCookie("bodyBackground");
    else{
        var bodyBackground = "background-image: url(\"../pic4.jpg\");height:100%;opacity=0.3;";
        document.cookie = "bodyBackground="+bodyBackground;
        document.getElementsByTagName("body")[0].style = bodyBackground;
    }
    if ( $( ".text" ).length ) {
        if (getCookie("titleTheme") != null) {
            var t = document.getElementsByClassName("text");
            for (var i = 0; i < t.length; i++) {
                t[i].style += getCookie("titleTheme");
            }
        }
        else {
            var title = "color:black;";
            document.cookie = "titleTheme=" + title;
            var t = document.getElementsByClassName("text");
            for (var i = 0; i < t.length; i++) {
                t[i].style += title;
            }
        }
    }
    if ( $( ".btn" ).length ) {
        if (getCookie("btnTheme") != null) {
            var b = document.getElementsByClassName("btn");
            for (var i = 0; i < b.length; i++) {
                b[i].className = getCookie("btnTheme");
            }
        }
        else {
            var btnTheme = "btn btn-primary";
            document.cookie = "btnTheme=" + btnTheme;
            var b = document.getElementsByClassName("btn");
            for (var i = 0; i < b.length; i++) {
                b[i].className = btnTheme;
            }
        }
    }
    if ( $( "#plusbtn" ).length ) {
        if (getCookie("plusTheme") != null)
            document.getElementById("plusbtn").style = getCookie("plusTheme");
        else {
            var plusTheme = "color:SteelBlue;border-color:white;";
            document.getElementById("plusbtn").style = plusTheme;
            document.cookie = "plusTheme=" + plusTheme;
        }
    }
    if ( $( "#dropzone-area" ).length ) {
        if (getCookie("dropzoneTheme") != null)
            document.getElementById("dropzone-area").style = getCookie("dropzoneTheme");

        else {
            var dropzoneTheme = "border-color:SteelBlue;border-style:dotted;border-width:5px;";
            document.getElementById("dropzone-area").style = dropzoneTheme;
            document.cookie = "dropzoneTheme=" + dropzoneTheme;
        }
    }
    // if(getCookie("panelTheme") != null){
    //     var p = document.getElementsByClassName("panel");
    //     for (var i = 0; i < p.length; i++) {
    //         p[i].className =  getCookie("panelTheme");
    //     }
    // }
    // else{
    //     var panelTheme =  "row panel panel-info";
    //     document.cookie = "panelTheme="+panelTheme;
    //     var p = document.getElementsByClassName("panel");
    //     for (var i = 0; i < p.length; i++) {
    //         p[i].className = panelTheme;
    //     }
    // }
}
function setDefaultTheme(){
    var navbarTheme = "navbar navbar-default navbar-fixed-top";
    document.getElementById("navbar").className = navbarTheme;
    document.cookie = "navbarTheme="+navbarTheme;

    if ( $( "#plusbtn" ).length ) {
        var plusTheme =  "color:SteelBlue;border-color:white;";
        document.getElementById("plusbtn").style = plusTheme;
        document.cookie = "plusTheme="+plusTheme;
    }

    if ( $( "#dropzone-area" ).length ) {
        var dropzoneTheme =  "border-color:SteelBlue;border-style:dotted;border-width:5px;";
        document.getElementById("dropzone-area").style = dropzoneTheme;
        document.cookie = "dropzoneTheme="+dropzoneTheme;
    }


    // var panelTheme =  "row panel panel-info";
    // document.cookie = "panelTheme="+panelTheme;
    // var p = document.getElementsByClassName("panel");
    // for (var i = 0; i < p.length; i++) {
    //     p[i].className = panelTheme;
    // }

    var bodyBackground = "background-image: url(\"../pic4.jpg\");height: 100%;opacity=0.3;";
    document.cookie = "bodyBackground="+bodyBackground;
    document.getElementsByTagName("body")[0].style = bodyBackground;

    if ( $( ".text" ).length ) {
        var title = "color:black;";
        document.cookie = "titleTheme="+title;
        var t = document.getElementsByClassName("text");
        for (var i = 0; i < t.length; i++) {
            t[i].style += title;
        }
    }

    if ( $( ".btn" ).length ) {
        var btnTheme =  "btn btn-primary";
        document.cookie = "btnTheme="+btnTheme;
        var b = document.getElementsByClassName("btn");
        for (var i = 0; i < b.length; i++) {
            b[i].className = btnTheme;
        }
    }
    //alert(document.cookie);
}
function setInverseTheme(){

    var navbarTheme ="navbar navbar-inverse navbar-fixed-top";
    document.getElementById("navbar").className = navbarTheme;
    document.cookie = "navbarTheme="+navbarTheme;

    if ( $( "#plusbtn" ).length ) {
        var plusTheme =  "color:darkred;border-color:white;";
        document.getElementById("plusbtn").style = plusTheme;
        document.cookie = "plusTheme="+plusTheme;
    }

    if ( $( "#dropzone-area" ).length ) {
        var dropzoneTheme =  "border-color:darkred;border-style:dotted;border-width:5px;";
        document.getElementById("dropzone-area").style = dropzoneTheme;
        document.cookie = "dropzoneTheme="+dropzoneTheme;
    }
    //
    // var panelTheme =  "row panel panel-danger";
    // document.cookie = "panelTheme="+panelTheme;
    // var p = document.getElementsByClassName("panel");
    // for (var i = 0; i < p.length; i++) {
    //     p[i].className = panelTheme;
    // }

    var bodyBackground = "background-image: url(\"../pic2.jpg\");background-size: 100%;";
    document.cookie = "bodyBackground="+bodyBackground;
    document.getElementsByTagName("body")[0].style = bodyBackground;

    if ( $( ".text" ).length ) {
        var title = "color:white;";
        document.cookie = "titleTheme="+title;
        var t = document.getElementsByClassName("text");
        for (var i = 0; i < t.length; i++) {
            t[i].style += title;
        }
    }

    if ( $( ".btn" ).length ) {
        var btnTheme =  "btn btn-danger";
        document.cookie = "btnTheme="+btnTheme;
        var b = document.getElementsByClassName("btn");
        for (var i = 0; i < b.length; i++) {
            b[i].className = btnTheme;
        }
    }
    //alert(document.cookie);
}
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

