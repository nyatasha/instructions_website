/**
 * Created by Natallia on 09.03.2017.
 */
$('.toggler').click(function() {
    var $this = $(this);
    var toggled = $this.hasClass("fa fa-ban");
    $('div.vertical-nav').animate({
        'right': toggled ? -250 : 0
    });
    $this.toggleClass("fa fa-ban");
});
function fill(user){
    var jsonData = JSON.parse(user);
    for (var i = 0; i < jsonData.userList.length; i++) {
        var theUser = jsonData.userList[i].username;
        var number = jsonData.userList[i].id;
        var isLocked = jsonData.userList[i].isLocked;
        var isAdmin = jsonData.userList[i].isAdmin;
        console.log(isAdmin);
        var tr = $('<tr id=\"'+number+'\"/>').appendTo('.table-body');
        var th = $('<th scope="row">'+number+'</th>').appendTo(tr);
//console.log(typeof isLocked);
        var tempLocked;
        if(isLocked==true)
        {
         tempLocked='Banned';
        }else{
            tempLocked='Not banned';
        }
        var tempAdmin;
        if(isAdmin==true)
        {
            tempAdmin='Admin';
        }else{
            tempAdmin='User';
        }


        var td = $('<td>'+theUser+'</td>').appendTo(tr);
        var tdlocked = $('<td id="lockTemp'+number+'">'+tempLocked+'</td>').appendTo(tr);
        var tdlocked = $('<td id="adminTemp'+number+'">'+tempAdmin+'</td>').appendTo(tr);
        var tdBtn = $('<td><a class="btn" onclick="myclick(this.id)" id=\"'+number+'\">Ban user<i class="fa toggler" aria-hidden="true"></i></a></td>').appendTo(tr);
        var tdBtn = $('<td><a class="btn" onclick="clickAdmin(this.id)" id=\"'+number+'\">Make admin<i class="fa toggler" aria-hidden="true"></i></a></td>').appendTo(tr);

    }
}

function myclick(id){
   $.post("/ban?id="+id.toString());
    if(document.getElementById('lockTemp'+id.toString()).innerHTML == 'Banned'){
        document.getElementById('lockTemp'+id.toString()).innerHTML = "Not banned";
    }else{
        document.getElementById('lockTemp'+id.toString()).innerHTML = "Banned";
    }
}

function clickAdmin(id) {
    $.post("/sudo?id="+id.toString());
    if(document.getElementById('adminTemp'+id.toString()).innerHTML == 'Admin'){
        document.getElementById('adminTemp'+id.toString()).innerHTML = "User";
    }else{
        document.getElementById('adminTemp'+id.toString()).innerHTML = "Admin";
    }
}
