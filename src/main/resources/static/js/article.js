/**
 * Created by Natallia on 11.03.2017.
 */

function pdf(){
    var pdf = new jsPDF('p', 'pt', 'letter');
    pdf.addHTML($('#stepsList')[0], function () {
        pdf.save('Test.pdf');
    });
    // var doc = new jsPDF();
    // var imgData = 'data:image/jpeg;base64,'+ Base64.encode('yourimage.jpeg');
    // doc.addImage(imgData, 'JPEG', 15, 40, 180, 160);
    // var specialElementHandlers = {
    //     '#editor': function (element, renderer) {
    //         return true;
    //     }
    // };
    // doc.fromHTML($('#stepsList').html(), 15, 15, {
    //     'width': 170,
    //     'elementHandlers': specialElementHandlers
    // });
    // doc.save('sample-file.pdf');
}

function addArticles(id){
    $.ajax({
        url: '/show_article',
        type: 'POST',
        data: id,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(resposeJsonObject){
            location.href="article";
        }
    });

}



function showArticle(art){
    var jsonData = JSON.parse(art);
    document.getElementById('articleTitle').innerHTML = jsonData.articleTitle;
    document.getElementById('buttonLike').innerHTML='<span class="glyphicon glyphicon-thumbs-up fa-lg"> ' +jsonData.countLikes+ '</span>';
    document.getElementById('buttonDisLike').innerHTML = '<span class="glyphicon glyphicon-thumbs-down fa-lg"> ' +jsonData.countDislikes+ '</span>';
    $("#youtube").val(jsonData.videoURL);
    document.getElementsByClassName('container step')[0].id=jsonData.articleid;
    for (var i = 0; i < jsonData.stepList.length; i++) {
        var title = jsonData.stepList[i].title;
        var img = jsonData.stepList[i].img;
        var number = i+1;

        var panel = $('<div class="step" style="background-color: rgba(255,255,255,0.5);width:450px;height:450px;text-align:center;"/>').appendTo('.stepsList');
        var stepText =  $('<h2/>').appendTo(panel);
        stepText.append("Step "+number);
        panel.append('<img src=\"'+img+'\" width="300px" height="300px"/>');
        var titleText =  $('<h3 style="overflow: hidden;"/>').appendTo(panel);
        titleText.append(title);
    }
    document.getElementById('author').innerHTML = jsonData.author +"  "+jsonData.date;
}
function deleteInstruction(){
    var id = document.getElementsByClassName("container step")[0].id;
    //alert(id +"id");
    $.ajax({
        url: '/delete_article',
        type: 'POST',
        data: id, //Stringified Json Object
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(resposeJsonObject){

        }
    });
    $('#deleteModal').modal('show');
}

function clickLikes(id) {
    var thisArticleId = document.getElementsByClassName('container step')[0].id;
    console.log(typeof (id) + id);
    if (id.toString() == "buttonLike") {
        var text = $.post("/setLike?articleId=" + thisArticleId.toString() + "&value=" + 1);
        text.done(function (data) {
            document.getElementById('buttonDisLike').innerHTML = '<span class="glyphicon glyphicon-thumbs-down fa-lg">' + JSON.parse(data).countDisLikes + '</span>';
            document.getElementById('buttonLike').innerHTML = '<span class="glyphicon glyphicon-thumbs-up fa-lg">' + JSON.parse(data).countLikes + '</span>';
        });

        console.log(text.responseText);
    } else if (id.toString() == "buttonDisLike") {
        var text = $.post("/setLike?articleId=" + thisArticleId.toString() + "&value=" + 2);
        text.done(function (data) {
            document.getElementById('buttonLike').innerHTML = '<span class="glyphicon glyphicon-thumbs-up fa-lg" >' + JSON.parse(data).countLikes + '</span>';
            document.getElementById('buttonDisLike').innerHTML = '<span class="glyphicon glyphicon-thumbs-down fa-lg">' + JSON.parse(data).countDisLikes + '</span>';
        });
    }
}
