/**
 * Created by Natallia on 04.03.2017.
 */
if( $("#xs-check").is(":visible") )
    $("#collapsible").removeClass("in");

function addFresh(articles){
    var jsonData = JSON.parse(articles);
    for (var i = jsonData.articleList.length - 1; i >=0 ; i--) {
        //alert(jsonData.articleList[i].id);
        var title = jsonData.articleList[i].title;
        var img = jsonData.articleList[i].img;
        var id = jsonData.articleList[i].id;
        var number = i+1;
        //alert(title+"\n"+img);
        var figDiv = $('<div class="freshArticles"/>').appendTo('.freshNew');
        var figure = $('<figure/>').appendTo(figDiv);
        figure.append('<img onclick="addArticles(this.alt)" class="freshImgs" src=\"'+img+'\" alt=\"'+id+'\" width="200" height="200">');
        figure.append('<figcaption width="200px" height="30px"><h3>'+title+'</h3></figcaption>');
    }
}
function loadRandom(articles){
    console.log(articles);
    var jsonData = JSON.parse(articles);

    var artTitle = jsonData.articleTitle;
    var id=jsonData.articleid;

    var titleArticle = $('<h2 style="overflow:hidden;">'+artTitle+'</h2>').appendTo('.bg-info');
    var head = $('<div id="carousel-example-vertical" class="carousel vertical slide" style="margin-left: 10px"/>').appendTo('.bg-info');
    var box = $('<div class="carousel-inner" role="listbox">').appendTo(head);

    var item = $('<div class="item active"/>').appendTo('.carousel-inner');
    var headline = $('<p class="ticker-headline"/>').appendTo(item);
    var href = $(' <a onclick="addArticles(this.alt)" id=\"'+id+'\">').appendTo(headline);
    href.append('<img src=\"'+jsonData.stepList[0].img+'\" style="width:200px;cursor: pointer;height: 200px;" class="thumbnail imgCarousel"/>');
    href.append(' <strong>'+jsonData.stepList[0].title+'</strong>');

    for (var i = 1; i < jsonData.stepList.length; i++) {
        var title = jsonData.stepList[i].title;
        var img = jsonData.stepList[i].img;

        var item = $('<div class="item"/>').appendTo('.carousel-inner');
        var headline = $('<p class="ticker-headline"/>').appendTo(item);
        var href = $(' <a onclick="addArticles(this.alt)" id=\"'+id+'\">').appendTo(headline);
        href.append('<img src=\"'+img+'\" class="thumbnail imgCarousel" style="width:200px;cursor: pointer;height: 200px;" />');
        href.append(' <strong>'+title+'</strong>');
    }

    var verticalControl = $('<a class="up carousel-control" href="#carousel-example-vertical" role="button" data-slide="prev" '+
        'style="padding: 0px"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>'+
        '<span class="sr-only">Previous</span></a>').appendTo(head);
    var horizControl = $('<a class="down carousel-control" href="#carousel-example-vertical" role="button" data-slide="next"'+
    'style="padding: 0px"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>'+
        '<span class="sr-only">Next</span></a>').appendTo(head);

}
function addPopular(articles){
    var jsonData = JSON.parse(articles);
    for (var i = jsonData.articleList.length - 1; i >=0; i--) {
        //alert(jsonData.articleList[i].id);
        var title = jsonData.articleList[i].title;
        var img = jsonData.articleList[i].img;
        var id = jsonData.articleList[i].id;
        var number = i+1;
        //alert(title+"\n"+img);
        var figDiv = $('<div class="popularArticles"/>').appendTo('.popular');
        var figure = $('<figure/>').appendTo(figDiv);
        figure.append('<img onclick="addArticles(this.alt)" class="popularImgs" src=\"'+img+'\" alt=\"'+id+'\" width="200" height="200">');
        figure.append('<figcaption width="200px" height="30px"><h3>'+title+'</h3></figcaption>');
    }
}