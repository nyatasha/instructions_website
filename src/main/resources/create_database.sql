-- table users with id and names(tokens) od users
DROP TABLE IF EXISTS users,articles,steps,roles,userRoles,categories,tags;
CREATE TABLE users (
  id       INT          NOT NULL  AUTO_INCREMENT PRIMARY KEY,
  username TEXT NOT NULL,
  uniqID  VARCHAR(255) NOT NULL,
  isLocked BOOLEAN NOT NULL
)
  ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;


CREATE TABLE categories(
  category_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  category_en VARCHAR(255) NOT NUll,
  category_ru VARCHAR(255) NOT NULL
)
  ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

-- table Articles. Contains the information about articles.
--  Likes: 1-Like, 0-ignore,(-1)-dislike.
CREATE TABLE articles (
  id    INT    NOT NULL  AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255)   NOT NULL,
  date  DATE   NOT NULL,
  videoURL VARCHAR(255) NOT NULL,
  category INT NOT NULL,
  FOREIGN KEY(category) REFERENCES categories(category_id),
  user_id INT NOT NULL,
  FOREIGN KEY(user_id) REFERENCES users(id)
)
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

CREATE TABLE likes(
  likeId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  articleId INT NOT NULL,
  userId INT NOT NULL,
  value INT NOT NULL,
  FOREIGN KEY (articleId) REFERENCES articles(id),
  FOREIGN KEY (userId) REFERENCES users(id)
)
  ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_unicode_ci;

-- table Steps&Instruction - contains information for definite step.

CREATE TABLE steps (
  idStep     INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idArticles INT NOT NULL,
  position   INT NOT NULL,
  url        VARCHAR(255),
  stepTitle  VARCHAR(255)
)
  ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

-- Table for userRoles. 1-admin, 2-authorized user, 3-unauthorised user.
CREATE TABLE roles (
  id   INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL
)
  ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

-- Table for users and their roles.

CREATE TABLE userRoles (
  user_id INT NOT NULL,
  role_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (role_id) REFERENCES roles (id)
)
  ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE tags(
  tag_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  tag_name VARCHAR(255) NOT NULL
)
  ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE tagsArticles (
  tag_id INT NOT NULL,
  article_id INT NOT NULL,
  FOREIGN KEY (tag_id) REFERENCES tags (tag_id),
  FOREIGN KEY (article_id) REFERENCES articles (id)
)
  ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;



-- Insert data Change it in conformity with your acc in VK(Fb,Tw).
INSERT INTO roles VALUES (1, "ROLE_ADMIN");
INSERT INTO roles VALUES (2, "ROLE_USER");
INSERT INTO tags VALUES (1,"moony");
INSERT INTO tags VALUES (2,"wormtail");
INSERT INTO tags VALUES (3,"paddfoot");
INSERT INTO tags VALUES (4,"prongs");

INSERT INTO categories VALUES(1,"Home and village","Дом и дача");
INSERT INTO categories VALUES(2,"Presents","Подарки");
INSERT INTO categories VALUES(3,"Cookery","Кулинария");
INSERT INTO categories VALUES(4,"Needlework","Рукоделие");
INSERT INTO categories VALUES(5,"Programming","Программирование");
INSERT INTO categories VALUES(6,"Cars","Автомобили");
INSERT INTO categories VALUES(7,"Weapon","Оружие");
INSERT INTO categories VALUES(8,"Modeling","Моделирование");



INSERT INTO userRoles VALUES (1, 1);