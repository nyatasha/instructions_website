$.get("/getTagNames", function(data){
    $.each(data,function (i,categories) {
        $('#ptTags').append($('<a href="#" onclick="return false">' + categories + '</a>'));
    });
});



window.onload = function () {
    TagCanvas.textFont = 'Impact,"Arial Black",sans-serif';
    TagCanvas.textColour = '#0d983d';
    TagCanvas.textHeight = 25;
    TagCanvas.outlineColour = '#f6f';
    TagCanvas.outlineThickness = 3;
    TagCanvas.outlineOffset = 5;
    TagCanvas.outlineMethod = 'outline';
    TagCanvas.maxSpeed = 0.06;
    TagCanvas.minBrightness = 0.2;
    TagCanvas.depth = 0.95;
    TagCanvas.pulsateTo = 0.2;
    TagCanvas.pulsateTime = 0.75;
    TagCanvas.decel = 0.9;
    TagCanvas.reverse = true;
    TagCanvas.shadow = '#336';
    TagCanvas.shadowBlur = 3;
    TagCanvas.shadowOffset = [1, 1];
    TagCanvas.wheelZoom = false;
    TagCanvas.fadeIn = 800;
    try {
        TagCanvas.Start('tagcanvas1', 'ptTags');
    } catch (e) {
    }
};